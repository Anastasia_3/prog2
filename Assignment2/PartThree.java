
import java.util.Scanner;
class PartThree{
  public static void main(String[]args){
	 Scanner user =new Scanner(System.in);
	 System.out.println("Enter square length");
	 double squareLength= user.nextDouble();
	 System.out.println("Enter rectangle length");
	 double rectangleLength= user.nextDouble();
	 System.out.println("Enter rectangle width");
	 double rectangleWidth= user.nextDouble();
	 
	 AreaComputations sc = new AreaComputations();
	 System.out.println("Square area is "+AreaComputations.areaSquare(squareLength));
	 System.out.println("Rectangle area is "+sc.areaRectangle(rectangleLength,rectangleWidth));
 }
}