
class MethodsTest{
 public static void main(String[]args){
  SecondClass sc = new SecondClass();
  int x=10;
  System.out.println(SecondClass.addOne(50));
  System.out.println(sc.addTwo(50));
 }
 public static void methodNoInputNoReturn(){
  System.out.println("I'm in a method that takes no input and returns nothing");
  int x=50;
  System.out.println(x);
 }
 public static void methodOneInputNoReturn(int x){
  System.out.println("Inside the method one input no return");
  System.out.println(x);
 }
 public static void methodTwoInputNoReturn(int x, double y){
  System.out.println("Inside the method two inputs no return");
  System.out.println(x+"  "+y);
 }
 public static int methodNoInputReturnInt (){
  return 6;
 }
 public static double sumSquareRoot (int x, int y){
  int sum=x+y;
  return Math.sqrt(sum);
 }
}