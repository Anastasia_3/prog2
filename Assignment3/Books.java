public class Books{
	
	public String title;
	public String author;
	public int year;
	
	public void displayBook(){
		System.out.println(title+" by "+author+" "+year);
	}
}