import java.util.Scanner;
public class Shop{
	public static void main(String[]args){
		Scanner user=new Scanner(System.in);
		Books[] books=new Books[4];
		
	
		for(int count=0;count<books.length;count++){
			System.out.println("Book Title: ");
			String bTitle=user.next();
			System.out.println("Author: ");
			String bAuthor=user.next();
			System.out.println("Release year: ");
			int bYear=user.nextInt();
			books[count]=new Books();
			books[count].getYear(bYear);
		    books[count].getTitle(bTitle);
		}
		
		books[books.length-1].displayBook();//before setting fields to different values
		books[books.length-1].getYear(2014);
		books[books.length-1].getTitle("Psychology");
		books[books.length-1].displayBook();
	}
}