public class Books{
	
	private String title;
	private String author;
	private int year;
	
	public void displayBook(){
		System.out.println(title+" by "+author+" "+year);
	}
	public String getTitle(String bookTitle){
		this.title=bookTitle;
		return this.title;
	}
	public int getYear(int publishingYear){
		this.year=publishingYear;
		return this.year;
	}
	public Book2(String bTitle, String bAuthor, int bYear){
		this.title=bTitle;
		this.author=bAuthor;
		this.year=bYear;
	}
}